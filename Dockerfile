FROM amazoncorretto:11-alpine-jdk
COPY build/libs/data-extractor-1.0.0.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]