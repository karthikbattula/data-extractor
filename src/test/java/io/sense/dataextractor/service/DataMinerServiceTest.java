package io.sense.dataextractor.service;

import io.sense.dataextractor.model.AssetData;
import io.sense.dataextractor.model.AssetData.Asset;
import org.apache.commons.collections4.ListUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.eq;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@RunWith(MockitoJUnitRunner.class)
public class DataMinerServiceTest {

    public static final String DATA_PROVIDER_URL = "https://reference.intellisense.io/test.dataprovider";

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks private DataMinerService dataMinerService;

    @Before
    public void setUp(){
        ReflectionTestUtils.setField(dataMinerService, "dataProviderUrl", DATA_PROVIDER_URL);
    }

    @Test
    public void testGetAssetDataFor2min(){
        // GIVEN
        AssetData.Asset asset = AssetData.Asset.builder().metric3000(asList(4.123146, 4.5846, 5.646, 8.54646, 9.546)).build();
        AssetData assetData = AssetData.builder().asset660(asset).build();

        // WHEN
        Mockito.when(restTemplate.getForObject(eq(DATA_PROVIDER_URL), eq(AssetData.class))).thenReturn(assetData);

        // THEN
        AssetData responseAssetData = dataMinerService.getAssetData(2);
        assertNotNull(responseAssetData);
        assertEquals(2, responseAssetData.getAsset660().getMetric3000().size());
    }

    @Test(expected = ResponseStatusException.class)
    public void throwExceptionWhenInvokingDataProvider(){
        // WHEN
        Mockito.when(restTemplate.getForObject(eq(DATA_PROVIDER_URL), eq(AssetData.class))).thenThrow(new ResponseStatusException(INTERNAL_SERVER_ERROR));

        // THEN
        dataMinerService.getAssetData(2);
    }

}
