package io.sense.dataextractor.resource;

import io.sense.dataextractor.model.AssetData;
import io.sense.dataextractor.model.DataMiningRequest;
import io.sense.dataextractor.service.DataMinerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.OK;

@RunWith(MockitoJUnitRunner.class)
public class DataMinerTest {

    @Mock
    private DataMinerService dataMinerService;

    @InjectMocks private DataMiner dataMiner;

    @Test
    public void testGetAssetData(){
        // GIVEN
        DataMiningRequest dataMiningRequest = new DataMiningRequest();
        dataMiningRequest.setPeriod(60);
        AssetData.Asset asset = AssetData.Asset.builder().metric3000(asList(4.123146, 4.5846)).build();
        AssetData assetData = AssetData.builder().asset660(asset).build();

        //WHEN
        when(dataMinerService.getAssetData(eq(60))).thenReturn(assetData);

        //THEN
        ResponseEntity<AssetData> response = dataMiner.extractData(dataMiningRequest);
        assertEquals(OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertNotNull(response.getBody().getAsset660());
        assertNotNull(response.getBody().getAsset660().getMetric3000());
    }

    @Test(expected = ResponseStatusException.class)
    public void validatePeriodThrowException(){
        // GIVEN
        DataMiningRequest dataMiningRequest = new DataMiningRequest();
        dataMiningRequest.setPeriod(null);

        //THEN
        dataMiner.extractData(dataMiningRequest);
    }
}
