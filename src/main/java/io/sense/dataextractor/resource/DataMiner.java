package io.sense.dataextractor.resource;

import io.sense.dataextractor.model.AssetData;
import io.sense.dataextractor.model.DataMiningRequest;
import io.sense.dataextractor.service.DataMinerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

@RestController
public class DataMiner {

    @Autowired private DataMinerService dataMinerService;

    @PostMapping(path = "/data-miner", produces = "application/json", consumes = "application/json")
    public ResponseEntity<AssetData> extractData(@RequestBody DataMiningRequest dataMiningRequest){
        validate(dataMiningRequest);
        AssetData assetData = dataMinerService.getAssetData(dataMiningRequest.getPeriod());
        return new ResponseEntity<>(assetData, OK);
    }

    private void validate(DataMiningRequest dataMiningRequest){
        if(dataMiningRequest.getPeriod() == null || dataMiningRequest.getPeriod() <= 0){
            throw new ResponseStatusException(BAD_REQUEST, "error.client.invalid.period");
        }
    }
}
