package io.sense.dataextractor.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AssetData {

    @JsonProperty("660")
    private Asset asset660;

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Asset{

        @JsonProperty("3000")
        private List<Double> metric3000;

        @JsonProperty("3001")
        private List<Double> metric3001;

        @JsonProperty("3002")
        private List<Double> metric3002;

        @JsonProperty("3003")
        private List<Double> metric3003;

        @JsonProperty("3004")
        private List<Double> metric3004;

        @JsonProperty("3005")
        private List<Double> metric3005;

        @JsonProperty("time")
        private List<String> time;
    }
}
