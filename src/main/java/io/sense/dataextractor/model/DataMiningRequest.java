package io.sense.dataextractor.model;

import lombok.Data;

@Data
public class DataMiningRequest {
    private Integer period;
}
