package io.sense.dataextractor.service;

import io.sense.dataextractor.model.AssetData;
import io.sense.dataextractor.model.AssetData.Asset;
import org.apache.commons.collections4.ListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@Service
public class DataMinerService {

    @Autowired private RestTemplate restTemplate;

    @Value("${sense.data-provider}")
    private String dataProviderUrl;

    public AssetData getAssetData(Integer period){
        AssetData assetData = invokeDataProvider();
        Asset asset = assetData.getAsset660();

        Asset asset660 = Asset.builder()
                .metric3000(average(partition(asset.getMetric3000(), period)))
                .metric3001(average(partition(asset.getMetric3001(), period)))
                .metric3002(average(partition(asset.getMetric3002(), period)))
                .metric3003(average(partition(asset.getMetric3003(), period)))
                .metric3004(average(partition(asset.getMetric3004(), period)))
                .metric3005(average(partition(asset.getMetric3005(), period)))
                .time(filterTimeStamps(partition(asset.getTime(), period)))
                .build();
        return AssetData.builder().asset660(asset660).build();
    }

    private AssetData invokeDataProvider(){
        try {
            return restTemplate.getForObject(dataProviderUrl, AssetData.class);
        } catch (Exception ex){
            throw new ResponseStatusException(INTERNAL_SERVER_ERROR, "Error fetching data from Data provider");
        }
    }

    /**
     * Partitions the given list based on the given count and
     * returns the lists with size matching the given count
     * @param data
     * @param count
     * @param <T>
     * @return
     */
    private <T> List<List<T>> partition(List<T> data, Integer count){
        if(isNotEmpty(data)){
            return ListUtils.partition(data, count)
                    .stream()
                    .filter(l -> l.size() == count)
                    .collect(Collectors.toList());
        }
        return emptyList();
    }

    /**
     * Calculates the average metrics for each sublist which are partitioned based on given period minutes
     * @param metrics
     * @return
     */
    private List<Double> average(List<List<Double>> metrics){
        return metrics.stream()
                .map(sublist ->
                        sublist.stream()
                                .filter(Objects::nonNull)
                                .mapToDouble(m -> m).summaryStatistics().getAverage()
                ).collect(Collectors.toList());
    }

    /**
     * Filters timestamps and returns last timestamp in each sublist
     * @param timestamps
     * @return
     */
    private List<String> filterTimeStamps(List<List<String>> timestamps){
        return timestamps.stream()
                .map(sublist -> sublist.stream().filter(Objects::nonNull).reduce((first, second) -> second).orElse(null))
                .collect(Collectors.toList());
    }
}
