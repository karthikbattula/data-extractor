# Data Extractor Service

## Description
Take time series data from a REST API endpoint and aggregate the provided data from minute values into 10 minute, 30 minute, and 1 hour averaged values based on an aggregate time period specified in the request.

The data set is accessed from here: https://reference.intellisense.io/test.dataprovider
## Requirements

Technologies used:

- JDK 11
- Gradle 6.8
- Spring Boot 2.4.5
- Junit 4


## Building the Application
Application can be build and packaged in a docker image using any of below options
#### Option 1 
- Spring boot latest version uses built in module to generate docker images by executing the below gradle task from project root path
```
./gradlew bootBuildImage
```

#### Option 2 
- Traditional way of building docker image, by running below commands from project root path
  - Build the project by running the gradle task
    ```
    ./gradlew build
    ```
  - Build the docker image by running the docker command
    ```
    docker build --tag=data-extractor:1.0.0 . 
    ```

## Running the Application
Start the service by running the docker container for the image generated in above steps.

```
docker run -p9080:9080 data-extractor:1.0.0
```

This will expose the data miner service:
```
POST http://localhost:9080/data-miner

Content-Type: application/json

{
    "period": 60
}
```
